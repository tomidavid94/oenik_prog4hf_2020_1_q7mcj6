﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderPlacement.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<OrderPlacement.Data.Members, OrderPlacement.Web.Models.Member>().
                ForMember(dest => dest.MemberId, map => map.MapFrom(src => src.MemberID)).
                ForMember(dest => dest.AddressId, map => map.MapFrom(src => src.AddressID)).
                ForMember(dest => dest.Prefix, map => map.MapFrom(src => src.Prefix)).
                ForMember(dest => dest.FistName, map => map.MapFrom(src => src.FirstName)).
                ForMember(dest => dest.LastName, map => map.MapFrom(src => src.LastName));
            });

            //.ReveseMap() visszafordítás
            //ForMember(dest => dest.brands, map => map.MapFrom(src => logic.GetBrandByName(src.BrandName));

            return config.CreateMapper();
        }
    }
}