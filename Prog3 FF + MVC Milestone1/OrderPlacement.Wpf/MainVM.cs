﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OrderPlacement.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private MemberVM selectedMember;
        private ObservableCollection<MemberVM> allMembers;
        public ObservableCollection<MemberVM> Allmembers
        {
            get { return allMembers; }
            set { Set(ref allMembers, value); }
        }
        public MemberVM SelectedMember
        {
            get { return selectedMember; }
            set { Set(ref selectedMember, value); }
        }
        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<MemberVM, bool> EditorFunc { get; set; }

        public MainVM()
        {
            logic = new MainLogic();
            DelCmd = new RelayCommand(() => logic.ApiDelMember(selectedMember));
            AddCmd = new RelayCommand(() => logic.EditMember(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditMember(selectedMember, EditorFunc));
            LoadCmd = new RelayCommand(() => Allmembers = new ObservableCollection<MemberVM>(logic.ApiGetMembers()));
        }


    }
}
