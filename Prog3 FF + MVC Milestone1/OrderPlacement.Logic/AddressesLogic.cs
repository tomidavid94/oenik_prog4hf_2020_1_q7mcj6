﻿// <copyright file="AddressesLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Logic
{
    using System;
    using System.Linq;
    using OrderPlacement.Data;
    using OrderPlacement.Repository.Interfaces;

    /// <summary>
    /// Logic handling addresses
    /// </summary>
    public class AddressesLogic
    {
        private IAddressesRepository addressesRepository = new AddressesRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressesLogic"/> class.
        /// </summary>
        public AddressesLogic()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressesLogic"/> class.
        /// </summary>
        /// <param name="addressesRepository">Repository to access data</param>
        public AddressesLogic(IAddressesRepository addressesRepository)
        {
            this.addressesRepository = addressesRepository ?? throw new ArgumentNullException(nameof(addressesRepository));
        }

        /// <summary>
        /// Creates a new entity
        /// </summary>
        /// <param name="param">Inserted parameteres</param>
        public void New(string[] param)
        {
            Addresses addresses = new Addresses();
            for (int i = 0; i < 11; i++)
            {
                if (param[i] != null && !param[i].Trim(' ').Equals(string.Empty))
                {
                    switch (i)
                    {
                        case 0:
                            addresses.AddressDescription = param[i];
                            continue;
                        case 1:
                            addresses.City = param[i];
                            continue;
                        case 2:
                            addresses.Country = param[i];
                            continue;
                        case 3:
                            addresses.DoorNumber = param[i];
                            continue;
                        case 4:
                            addresses.FloorNumber = param[i];
                            continue;
                        case 5:
                            addresses.NumberFrom = param[i];
                            continue;
                        case 6:
                            addresses.NumberTo = param[i];
                            continue;
                        case 7:
                            addresses.Province = param[i];
                            continue;
                        case 8:
                            addresses.Street = param[i];
                            continue;
                        case 9:
                            addresses.StreetExtension = param[i];
                            continue;
                        case 10:
                            addresses.ZIP = param[i];
                            continue;
                        default:
                            break;
                    }
                }
            }

            this.Add(addresses);
        }

        /// <summary>
        /// Lists shit
        /// </summary>
        /// <returns>All addresses</returns>
        public string[] List()
        {
            var q = from w in this.addressesRepository.GetAll()
                select w;
            string[] res = new string[q.Count()];
            int i = 0;
            foreach (Addresses v in q)
            {
                string str = string.Empty;

                str += v.AddressID + " ";
                if (v.AddressDescription != null && !v.AddressDescription.Equals(string.Empty))
                {
                    str += v.AddressDescription + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.City != null && !v.City.Equals(string.Empty))
                {
                    str += v.City + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.Country != null && !v.Country.Equals(string.Empty))
                {
                    str += v.Country + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.DoorNumber != null && !v.DoorNumber.Equals(string.Empty))
                {
                    str += v.DoorNumber + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.FloorNumber != null && !v.FloorNumber.Equals(string.Empty))
                {
                    str += v.FloorNumber + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.NumberFrom != null && !v.NumberFrom.Equals(string.Empty))
                {
                    str += v.NumberFrom + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.NumberTo != null && !v.NumberTo.Equals(string.Empty))
                {
                    str += v.NumberTo + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.Province != null && !v.Province.Equals(string.Empty))
                {
                    str += v.Province + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.Street != null && !v.Street.Equals(string.Empty))
                {
                    str += v.Street + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.StreetExtension != null && !v.StreetExtension.Equals(string.Empty))
                {
                    str += v.StreetExtension + " ";
                }
                else
                {
                    str += " null ";
                }

                if (v.ZIP != null && !v.ZIP.Equals(string.Empty))
                {
                    str += v.ZIP + " ";
                }
                else
                {
                    str += " null ";
                }

                res[i] = str;
                i++;
            }

            return res;
        }

        /// <summary>
        /// Changes an entity in the table
        /// </summary>
        /// <param name="number">ID of entity</param>
        /// <param name="field">Number of field</param>
        /// <param name="newfield">Value of field's new value</param>
        public void Change(string number, string field, string newfield)
        {
            string fie = string.Empty;
            switch (int.Parse(field))
            {
                case 1:
                    fie = "AddressDescription";
                    break;
                case 2:
                    fie = "City";
                    break;
                case 3:
                    fie = "Country";
                    break;
                case 4:
                    fie = "DoorNumber";
                    break;
                case 5:
                    fie = "FloorNumber";
                    break;
                case 6:
                    fie = "NumberFrom";
                    break;
                case 7:
                    fie = "NumberTo";
                    break;
                case 8:
                    fie = "Province";
                    break;
                case 9:
                    fie = "Street";
                    break;
                case 10:
                    fie = "StreetExpansion";
                    break;
                case 11:
                    fie = "ZIP";
                    break;
                default:
                    break;
            }

            Addresses addresses = this.addressesRepository.GetAll().Where(w => w.AddressID.ToString().Equals(number)).Single();
            this.addressesRepository.Change(addresses, fie, newfield);
        }

        /// <summary>
        /// Removing an address
        /// </summary>
        /// <param name="id">Id of removed address</param>
        public void Remove(int id)
        {
            try
            {
                Addresses addresses = this.addressesRepository.GetAll().Where(x => x.AddressID == id).Single();
                this.addressesRepository.Remove(addresses);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (e.InnerException != null)
                {
                    if (e.InnerException.InnerException != null)
                    {
                        Console.WriteLine(e.InnerException.InnerException.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Adds a new address to the table
        /// </summary>
        /// <param name="addresses">Input address</param>
        private void Add(Addresses addresses)
        {
            this.addressesRepository.Insert(addresses);
        }
    }
}
