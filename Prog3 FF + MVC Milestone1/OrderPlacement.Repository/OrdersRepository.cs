﻿// <copyright file="OrdersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Repository
{
    using System;
    using System.Linq;
    using System.Threading;
    using OrderPlacement.Data;
    using OrderPlacement.Repository.Interfaces;

    /// <summary>
    /// Repository for accessing orders
    /// </summary>
    public class OrdersRepository : Repository, IOrdersRepository
    {
        private OrderPlacementTablesEntities dbEntity = new OrderPlacementTablesEntities();

        /// <summary>
        /// Changes an entity in the table
        /// </summary>
        /// <param name="entity">ID of entity</param>
        /// <param name="field">Number of field</param>
        /// <param name="newfield">Value of field's new value</param>
        public void Change(Orders entity, string field, string newfield)
        {
            int asd = 0;
            try
            {
                switch (field)
                {
                    case "MemberID":
                        try
                        {
                            asd = int.Parse(newfield);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            return;
                        }

                        entity.MemberID = asd;
                        break;
                    case "ItemID":
                        try
                        {
                            asd = int.Parse(newfield);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            return;
                        }

                        entity.ItemID = asd;
                        break;
                    case "OrderState":
                        entity.OrderState = newfield;
                        break;
                    case "GrossValue":
                        try
                        {
                            asd = int.Parse(newfield);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            return;
                        }

                        entity.GrossValue = asd;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            this.dbEntity.SaveChanges();
        }

        /// <summary>
        /// Returns all orders as queryable
        /// </summary>
        /// <returns>All orders</returns>
        public IQueryable<Orders> GetAll()
        {
            return this.dbEntity.Set<Orders>().AsQueryable();
        }

        /// <summary>
        /// Inserts a new order into the table
        /// </summary>
        /// <param name="entity">Inserted entity</param>
        public void Insert(Orders entity)
        {
            this.dbEntity.Orders.Add(entity);
            try
            {
                this.dbEntity.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (e.InnerException != null)
                {
                    if (e.InnerException.InnerException != null)
                    {
                        Console.WriteLine(e.InnerException.InnerException.Message);
                        Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                        while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                        {
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Remove a Member from the table
        /// </summary>
        /// <param name="entity">Removed order</param>
        public void Remove(Orders entity)
        {
            this.dbEntity.Orders.Remove(entity);
            this.dbEntity.SaveChanges();
        }
    }
}
