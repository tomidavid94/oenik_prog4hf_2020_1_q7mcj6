﻿// <copyright file="AddressesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Data
{
    using System;
    using System.Linq;
    using OrderPlacement.Repository;
    using OrderPlacement.Repository.Interfaces;

    /// <summary>
    /// Repository for accessing addresses
    /// </summary>
    public class AddressesRepository : Repository, IAddressesRepository
    {
        private OrderPlacementTablesEntities dbEntity = new OrderPlacementTablesEntities();

        /// <summary>
        /// Changes an entity in the table
        /// </summary>
        /// <param name="entity">ID of entity</param>
        /// <param name="field">Number of field</param>
        /// <param name="newfield">Value of field's new value</param>
        public void Change(Addresses entity, string field, string newfield)
        {
            try
            {
                switch (field)
                {
                    case "AddressDescription":
                        entity.AddressDescription = newfield;
                        break;
                    case "City":
                        entity.City = newfield;
                        break;
                    case "Country":
                        entity.Country = newfield;
                        break;
                    case "DoorNumber":
                        entity.DoorNumber = newfield;
                        break;
                    case "FloorNumber":
                        entity.FloorNumber = newfield;
                        break;
                    case "NumberFrom":
                        entity.NumberFrom = newfield;
                        break;
                    case "NumberTo":
                        entity.NumberTo = newfield;
                        break;
                    case "Province":
                        entity.Province = newfield;
                        break;
                    case "Street":
                        entity.Street = newfield;
                        break;
                    case "StreetExtension":
                        entity.StreetExtension = newfield;
                        break;
                    case "ZIP":
                        try
                        {
                            if (int.Parse(newfield.Substring(0, 1)) == 0)
                            {
                                throw new ArgumentOutOfRangeException();
                            }
                            else
                            {
                                entity.ZIP = newfield;
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                this.dbEntity.SaveChanges();
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    if (e.InnerException.InnerException != null)
                    {
                        Console.WriteLine(e.InnerException.InnerException.Message);
                    }
                    else
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Returns all addresses as queryable
        /// </summary>
        /// <returns>All addresses</returns>
        public IQueryable<Addresses> GetAll()
        {
            return this.dbEntity.Set<Addresses>().AsQueryable();
        }

        /// <summary>
        /// Inserts a new address into the table
        /// </summary>
        /// <param name="entity">Inserted entity</param>
        public void Insert(Addresses entity)
        {
            this.dbEntity.Addresses.Add(entity);
            this.dbEntity.SaveChanges();
        }

        /// <summary>
        /// Remove an address from the table
        /// </summary>
        /// <param name="entity">Removed address</param>
        public void Remove(Addresses entity)
        {
            this.dbEntity.Addresses.Remove(entity);
            this.dbEntity.SaveChanges();
        }
    }
}
