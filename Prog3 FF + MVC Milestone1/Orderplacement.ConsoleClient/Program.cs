﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Orderplacement.ConsoleClient
{
    class Program
    {
        public class Member
        {
            public int MemberId { get; set; }
            public int AddressId { get; set; }
            public string Prefix { get; set; }
            public string FistName { get; set; }
            public string LastName { get; set; }
            public DateTime BirthDate { get; set; }
            public char Status { get; set; }
            public override string ToString()
            {
                return $"ID= {MemberId}\t Prefix= {Prefix}\t FirstName= {FistName}\t LastName= {LastName}";
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();
            string url = "http://localhost:50321/api/MembersApi/";
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Member>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Member.Prefix), "Dr.");
                postData.Add(nameof(Member.FistName), "Kiss");
                postData.Add(nameof(Member.LastName), "Sándor");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Add: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int memberId = JsonConvert.DeserializeObject<List<Member>>(json).Single(x => x.LastName == "Sándor").MemberId;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Member.MemberId), memberId.ToString());
                postData.Add(nameof(Member.Prefix), "Dr.");
                postData.Add(nameof(Member.FistName), "Kiss");
                postData.Add(nameof(Member.LastName), "Dezső");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + memberId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }
        }
    }
}
