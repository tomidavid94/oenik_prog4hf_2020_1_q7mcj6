﻿// <copyright file="OrdersLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using OrderPlacement.Data;
    using OrderPlacement.Repository;
    using OrderPlacement.Repository.Interfaces;

    /// <summary>
    /// Logic behind handling orders
    /// </summary>
    public class OrdersLogic
    {
        private IOrdersRepository ordersRepository = new OrdersRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersLogic"/> class.
        /// </summary>
        public OrdersLogic()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrdersLogic"/> class.
        /// </summary>
        /// <param name="ordersRepository">Repository to access data</param>
        public OrdersLogic(IOrdersRepository ordersRepository)
        {
            this.ordersRepository = ordersRepository ?? throw new ArgumentNullException(nameof(ordersRepository));
        }

        /// <summary>
        /// Creates a new entity
        /// </summary>
        /// <param name="param">Inserted parameteres</param>
        public void New(string[] param)
        {
            Orders orders = new Orders();
            for (int i = 0; i < 4; i++)
            {
                if (param[i] != null && !param[i].Trim(' ').Equals(string.Empty))
                {
                    switch (i)
                    {
                        case 0:
                            try
                            {
                                orders.MemberID = int.Parse(param[i]);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                i = 5;
                                orders = null;
                                break;
                            }

                            continue;
                        case 1:
                            try
                            {
                                orders.ItemID = int.Parse(param[i]);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                i = 5;
                                orders = null;
                                break;
                            }

                            continue;
                        case 2:
                            orders.OrderState = param[i];
                            continue;
                        case 3:
                            try
                            {
                                orders.GrossValue = int.Parse(param[i]);
                                if (orders.GrossValue < 1)
                                {
                                    Console.WriteLine("Nem megfelelő érték");
                                    Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                    while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                    {
                                    }

                                    break;
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                                i = 5;
                                orders = null;
                                break;
                            }

                            orders.VAT = orders.GrossValue * 0.27;
                            orders.NetValue = orders.GrossValue + orders.VAT;
                            continue;
                        default:
                            break;
                    }
                }
            }

            if (orders != null)
            {
                this.Add(orders);
            }
            else
            {
                Console.WriteLine("Új rendelés hozzáadása nem sikerült!");
                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                {
                }
            }
        }

        /// <summary>
        /// Lists shit
        /// </summary>
        /// <returns>All addresses</returns>
        public string[] List()
        {
            var q = from w in this.ordersRepository.GetAll()
                    select w;
            string[] res = new string[q.Count()];
            int i = 0;
            foreach (Orders v in q)
            {
                string str = $"{v.OrdersID} {v.MemberID} ";
                str += $"{v.ItemID} ";

                if (v.OrderState != null && !v.OrderState.Equals(string.Empty))
                {
                    str += v.OrderState + " ";
                }
                else
                {
                    str += " null ";
                }

                str += v.GrossValue + " ";
                str += v.VAT + " ";
                str += v.NetValue + " ";
                res[i] = str;
                i++;
            }

            return res;
        }

        /// <summary>
        /// Removing an order
        /// </summary>
        /// <param name="id">Id of removed order</param>
        public void Remove(int id)
        {
            try
            {
                Orders orders = this.ordersRepository.GetAll().Where(x => x.OrdersID == id).Single();
                this.ordersRepository.Remove(orders);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                if (e.InnerException != null)
                {
                    if (e.InnerException.InnerException != null)
                    {
                        Console.WriteLine(e.InnerException.InnerException.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Changes an entity in the table
        /// </summary>
        /// <param name="number">ID of entity</param>
        /// <param name="field">Number of field</param>
        /// <param name="newfield">Value of field's new value</param>
        public void Change(string number, string field, string newfield)
        {
            string fie = string.Empty;
            switch (int.Parse(field))
            {
                case 1:
                    fie = "MemberID";
                    break;
                case 2:
                    fie = "ItemID";
                    break;
                case 3:
                    fie = "OrderState";
                    break;
                case 4:
                    fie = "GrossValue";
                    break;
                default:
                    break;
            }

            Orders orders = this.ordersRepository.GetAll().Where(w => w.OrdersID.ToString().Equals(number)).Single();
            this.ordersRepository.Change(orders, fie, newfield);
        }

        /// <summary>
        /// Gets the amount of orders per member
        /// </summary>
        /// <param name="count">Size of array</param>
        /// <returns>Int array</returns>
        public int[] MemberOrdersCount(int count)
        {
            int[] amounts = new int[count];
            var q = from w in this.ordersRepository.GetAll()
                    group w by w.MemberID into g
                    select new
                    {
                        ID = g.Key,
                        Amount = g.Count()
                    };
            int i = 0;
            foreach (var v in q)
            {
                amounts[i] = v.Amount;
                i++;
            }

            return amounts;
        }

        /// <summary>
        /// Gets the value of orders placed per member
        /// </summary>
        /// <param name="memb">Array of memberIDs</param>
        /// <param name="count">Size of array</param>
        /// <returns>Double array</returns>
        public double[] MemberOrdersSum(int[] memb, int count)
        {
            double[] amounts = new double[count];
            var q = from w in this.ordersRepository.GetAll()
                    group w by w.MemberID into g
                    select new
                    {
                        ID = g.Key,
                        Amount = g.Sum(x => x.NetValue)
                    };
            foreach (var v in q)
            {
                Console.WriteLine(v);
            }

            for (int i = 0; i < memb.Length; i++)
            {
                foreach (var v in q)
                {
                    if (v.ID == memb[i])
                    {
                        amounts[i] = v.Amount == null ? 0 : v.Amount.Value;
                        break;
                    }
                }
            }

            return amounts;
        }

        /// <summary>
        /// Returns a list of ItemIds for member with memberid of id
        /// </summary>
        /// <param name="id">MemberID</param>
        /// <returns>List of itemIds</returns>
        public List<int> MemberItemIds(int id)
        {
            List<int> rst = new List<int>();
            var q = this.ordersRepository.GetAll().Where(x => x.MemberID == id);
            foreach (var item in q)
            {
                rst.Add(item.ItemID);
            }

            return rst;
        }

        /// <summary>
        /// Adds a new address to the table
        /// </summary>
        /// <param name="orders">Order to add to the database</param>
        private void Add(Orders orders)
        {
            this.ordersRepository.Insert(orders);
        }
    }
}
