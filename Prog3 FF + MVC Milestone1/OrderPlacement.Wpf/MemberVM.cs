﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderPlacement.Wpf
{
    class MemberVM : ObservableObject
    {
        private int memberId;

        public int MemberId
        {
            get { return  memberId; }
            set {  Set(ref memberId, value); }
        }

        private string prefix;

        public string Prefix
        {
            get { return prefix; }
            set { Set(ref prefix, value); }
        }

        private string fistname;

        public string Fistname
        {
            get { return fistname; }
            set { Set(ref fistname,value); }
        }

        private string lastname;  

        public string Lastname
        {
            get { return lastname; }
            set { Set(ref lastname, value); }
        }

        public void CopyFrom(MemberVM other)
        {
            if (other == null)
            {
                return;
            }
            this.MemberId = other.MemberId;
            this.Prefix = other.Prefix;
            this.Lastname = other.Lastname;
            this.Fistname = other.Fistname;
        }

    }
}
