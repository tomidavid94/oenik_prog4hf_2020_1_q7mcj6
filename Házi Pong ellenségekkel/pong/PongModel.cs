﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pong
{
    class PongModel
    {
        public int Errors { get; set; }
        public MyShape Pad { get; set; }
        public MyShape Ball { get; set; }
        public List<Star> Stars { get; set; }
        public MyShape EnemyPad1 { get; set; }
        public MyShape EnemyPad2 { get; set; }
        public MyShape EnemyPad3 { get; set; }

        public PongModel()
        {
            Ball = new MyShape(Config.Width / 2, Config.Height / 2, Config.BallSize, Config.BallSize);
            Pad = new MyShape(Config.Width / 2, Config.Height - Config.PadHeight, Config.PadWidth, Config.PadHeight);
            EnemyPad1 = new MyShape(Config.Width / 3, 0 , Config.EnemyWidth, Config.EnemyHeight);
            EnemyPad2 = new MyShape(Config.Width -150, 0, Config.EnemyWidth, Config.EnemyHeight);
            EnemyPad3 = new MyShape(Config.Width / 2 , 0, Config.EnemyWidth, Config.EnemyHeight);
            Stars = new List<Star>();
        }
    }
}
