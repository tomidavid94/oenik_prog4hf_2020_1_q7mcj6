﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace pong
{
    public static class Config
    {
        public static Brush BorderBrush = Brushes.DarkGray;
        public static Brush BgBrush = Brushes.Cyan;

        public static Brush BallBg = Brushes.Yellow;
        public static Brush BallLine = Brushes.Red;
        public static Brush PadBg = Brushes.Brown;
        public static Brush PadLine = Brushes.Black;

        public static double Width = 700;
        public static double Height = 300;
        public static int BorderSize = 4;

        public static double EnemyHeight = 25;
        public static double EnemyWidth = 25;
        public static Brush EnemyBg = Brushes.DarkBlue;


        public static int BallSize = 20;
        public static int PadWidth = 100;
        public static int PadHeight = 20;

    }
}
