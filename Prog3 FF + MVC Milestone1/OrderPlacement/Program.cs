﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement
{
    /// <summary>
    /// Main program
    /// </summary>
    public class Program
    {
        private static void Main(string[] args)
        {
            Consol.Consol consol = new Consol.Consol();
            consol.Run();
        }
    }
}
