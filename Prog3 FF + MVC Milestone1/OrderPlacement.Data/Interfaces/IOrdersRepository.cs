﻿// <copyright file="IOrdersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Repository.Interfaces
{
    using OrderPlacement.Data;

    /// <summary>
    /// Interface for accessing Orders
    /// </summary>
    public interface IOrdersRepository : IRepository<Orders>
    {
    }
}
