﻿// <copyright file="Consol.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Consol
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading;
    using OrderPlacement.Logic;

    /// <summary>
    /// Runs all the console logic
    /// </summary>
    public class Consol
    {
        private readonly AddressesLogic aLogic;
        private readonly MembersLogic mLogic;
        private readonly OrdersLogic oLogic;
        private readonly StocksLogic sLogic;
        private readonly JavaLogic jLogic;
        private bool cont = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="Consol"/> class.
        /// Constructor
        /// </summary>
        public Consol()
        {
            this.aLogic = new AddressesLogic();
            this.mLogic = new MembersLogic();
            this.oLogic = new OrdersLogic();
            this.sLogic = new StocksLogic();
            this.jLogic = new JavaLogic();
        }

        /// <summary>
        /// Runs the console
        /// </summary>
        public void Run()
        {
            this.Menu(0, string.Empty);
        }

        private static void WriteOut(params string[] args)
        {
            Console.Clear();
            foreach (string str in args)
            {
                Console.WriteLine(str);
            }
        }

        private void Menu(int depth, params string[] args)
        {
            WriteOut(args);
            if (depth == -101)
            {
                this.cont = false;
                System.Threading.Thread.Sleep(2000);
            }

            if (depth == 0)
            {
                WriteOut("Üdvözlöm a rendszer nyílvántartó felületén!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések kezelése", "2. Felhasználók kezelése", "3. Címek kezelése", "4. Készletek kezelése", "5. Java végpont", "Escape: Kilépés");
            }

            while (this.cont)
            {
                ConsoleKeyInfo key = default(ConsoleKeyInfo);
                switch (depth)
                {
                    // FŐMENÜ
                    case 0:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                this.Menu(1, "Üdvözlöm a rendeléskezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések listázása", "2. Rendelés létrehozása", "3. Rendelés módosítása", "4. Rendelés törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D2:
                                this.Menu(2, "Üdvözlöm a felhasználókezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Felhasználók listázása", "2. Felhasználó létrehozása", "3. Felhasználó módosítása", "4. Felhasználó törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D3:
                                this.Menu(3, "Üdvözlöm a címkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Címek listázása", "2. Cím létrehozása", "3. Cím módosítása", "4. Cím törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D4:
                                this.Menu(4, "Üdvözlöm a készletkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Készletek listázása", "2. Készlet létrehozása", "3. Készlet módosítása", "4. Készlet törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D5:
                                this.Menu(5, "1. Java végponttól ajánlatkérés", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                    break;

                    // RENDELÉSEK
                    case 1:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                this.Menu(11, "Rendelések listázása!", "Kérem válasszon az alábbi menüpontok közül", "1. Összes rendelés listázása", "2. Egy személyhez tartozó rendelések száma.", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D2:
                                this.Menu(12, "Rendelés létrehozása!", "Kérem válasszon az alábbi menüpontok közül", "1. Új rendelés létrehozása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D3:
                                this.Menu(13, "Rendelés módosítása!", "Kérem válasszon az alábbi menüpontok közül", "1. Egy meglévő rendelés módosítása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D4:
                                this.Menu(14, "Rendelés törlése!", "Kérem válasszon az alábbi menüpontok közül", "1. Meglévő rendelés törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D5:
                                this.Menu(0, "Üdvözlöm a rendszer nyílvántartó felületén!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések kezelése", "2. Felhasználók kezelése", "3. Címek kezelése", "4. Készletek kezelése", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                    break;

                    // FELHASZNÁLÓK
                    case 2:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                this.Menu(21, "Felhasználók listázása!", "Kérem válasszon az alábbi menüpontok közül", "1. Összes felhasználó listázása", "2. Egy személyhez tartozó rendelések nettó értékösszege.", "3. Egy személyhez tartozó rendelt termékek leírásai.", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D2:
                                this.Menu(22, "Felhasználó létrehozása!", "Kérem válasszon az alábbi menüpontok közül", "1. Új felhasználó létrehozása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D3:
                                this.Menu(23, "Felhasználó módosítása!", "Kérem válasszon az alábbi menüpontok közül", "1. Egy meglévő felhasználó módosítása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D4:
                                this.Menu(24, "Felhasználó törlése!", "Kérem válasszon az alábbi menüpontok közül", "1. Meglévő felhasználó törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D5:
                                this.Menu(0, "Üdvözlöm a rendszer nyílvántartó felületén!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések kezelése", "2. Felhasználók kezelése", "3. Címek kezelése", "4. Készletek kezelése", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // CÍMEK
                    case 3:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                this.Menu(31, "Címek listázása!", "Kérem válasszon az alábbi menüpontok közül", "1. Összes cím listázása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D2:
                                this.Menu(32, "Cím létrehozása!", "Kérem válasszon az alábbi menüpontok közül", "1. Új cím létrehozása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D3:
                                this.Menu(33, "Cím módosítása!", "Kérem válasszon az alábbi menüpontok közül", "1. Egy meglévő cím módosítása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D4:
                                this.Menu(34, "Cím törlése!", "Kérem válasszon az alábbi menüpontok közül", "1. Meglévő cím törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D5:
                                this.Menu(0, "Üdvözlöm a rendszer nyílvántartó felületén!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések kezelése", "2. Felhasználók kezelése", "3. Címek kezelése", "4. Készletek kezelése", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // KÉSZLETEK
                    case 4:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                this.Menu(41, "Készletek listázása!", "Kérem válasszon az alábbi menüpontok közül", "1. Összes készlet listázása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D2:
                                this.Menu(42, "Készlet létrehozása!", "Kérem válasszon az alábbi menüpontok közül", "1. Új készlet létrehozása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D3:
                                this.Menu(43, "Készlet módosítása!", "Kérem válasszon az alábbi menüpontok közül", "1. Egy meglévő készlet módosítása", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D4:
                                this.Menu(44, "Készlet törlése!", "Kérem válasszon az alábbi menüpontok közül", "1. Meglévő készlet törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D5:
                                this.Menu(0, "Üdvözlöm a rendszer nyílvántartó felületén!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések kezelése", "2. Felhasználók kezelése", "3. Címek kezelése", "4. Készletek kezelése", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Java végpont
                    case 5:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut("Adja meg az értékeket az ajánlathoz: \nID\nDescription\nValue");
                                try
                                {
                                    WriteOut(this.jLogic.Ask(Console.ReadLine(), Console.ReadLine(), Console.ReadLine()));
                                }
                                catch (Exception e)
                                {
                                    WriteOut(e.Message);
                                }

                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0, "Üdvözlöm a rendszer nyílvántartó felületén!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések kezelése", "2. Felhasználók kezelése", "3. Címek kezelése", "4. Készletek kezelése", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.D5:
                                this.Menu(0, "Üdvözlöm a rendszer nyílvántartó felületén!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések kezelése", "2. Felhasználók kezelése", "3. Címek kezelése", "4. Készletek kezelése", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                            // Rendelések listázása
                            case 11:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.oLogic.List());
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                this.cont = false;
                                break;
                            case ConsoleKey.D2:
                                string[] rst = new string[this.mLogic.Count()];
                                for (int i = 0; i < rst.Length; i++)
                                {
                                    rst[i] = this.mLogic.IDs()[i].ToString();
                                    rst[i] += $" {this.mLogic.GetParameter(this.mLogic.IDs()[i], "LastName")}";
                                    rst[i] += $" has {this.oLogic.MemberOrdersCount(this.mLogic.Count())[i].ToString()} orders!";
                                }

                                WriteOut(rst);
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(1, "Üdvözlöm a rendeléskezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések listázása", "2. Rendelés létrehozása", "3. Rendelés módosítása", "4. Rendelés törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Rendelés létrehozása
                    case 12:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                string[] str = new string[4];
                                WriteOut("MemberID ItemID OrderState(char) GrossValue");
                                for (int i = 0; i < 4; i++)
                                {
                                    str[i] = Console.ReadLine();
                                }

                                this.oLogic.New(str);
                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(1, "Üdvözlöm a rendeléskezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések listázása", "2. Rendelés létrehozása", "3. Rendelés módosítása", "4. Rendelés törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Rendelés módosítása
                    case 13:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.oLogic.List());
                                Console.WriteLine("Enter the ID of the entry you want to change!");
                                string number = Console.ReadLine();
                                WriteOut("Enter the number of field you want to change!", "1: MemberID 2: ItemID 3:OrderState 4:GrossValue");
                                string field = Console.ReadLine();
                                Console.WriteLine("Enter the new value of the field!");
                                this.oLogic.Change(number, field, Console.ReadLine());
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(1, "Üdvözlöm a rendeléskezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések listázása", "2. Rendelés létrehozása", "3. Rendelés módosítása", "4. Rendelés törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Rendelés törlése
                    case 14:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.oLogic.List());
                                Console.WriteLine("Enter the ID of the entry you want to delete!");
                                int rem = 0;
                                try
                                {
                                    rem = int.Parse(Console.ReadLine());
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                this.oLogic.Remove(rem);
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(1, "Üdvözlöm a rendeléskezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Rendelések listázása", "2. Rendelés létrehozása", "3. Rendelés módosítása", "4. Rendelés törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Felhasználók listázása
                    case 21:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.mLogic.List());
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D2:
                                string[] rst = new string[this.mLogic.Count()];
                                double[] nets = this.oLogic.MemberOrdersSum(this.mLogic.IDs(), this.mLogic.Count());
                                for (int i = 0; i < rst.Length; i++)
                                {
                                    rst[i] = this.mLogic.IDs()[i].ToString();
                                    rst[i] += $" {this.mLogic.GetParameter(this.mLogic.IDs()[i], "LastName")}";
                                    rst[i] += $" has placed orders in total NET value of {nets[i].ToString()}!";
                                }

                                WriteOut(rst);
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D3:
                                string[] res = new string[this.mLogic.Count()];
                                List<int>[] itemIDs = new List<int>[this.mLogic.Count()];
                                for (int i = 0; i < res.Length; i++)
                                {
                                    res[i] = this.mLogic.IDs()[i].ToString();
                                    res[i] += $" {this.mLogic.GetParameter(this.mLogic.IDs()[i], "LastName")} has placed orders for items";
                                    itemIDs[i] = new List<int>();
                                    itemIDs[i].Add(this.mLogic.IDs()[i]);
                                    List<int> act = this.oLogic.MemberItemIds(this.mLogic.IDs()[i]);
                                    foreach (int id in act)
                                    {
                                        res[i] += $" {this.sLogic.GetParameter(id, "ItemDescription")}";
                                    }

                                    if (act.Count == 0)
                                    {
                                        res[i] += " nothing";
                                    }
                                }

                                WriteOut(res);
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(2, "Üdvözlöm a felhasználókezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Felhasználók listázása", "2. Felhasználó létrehozása", "3. Felhasználó módosítása", "4. Felhasználó törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Felhasználó létrehozása
                    case 22:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                string[] str = new string[6];
                                WriteOut("AddressID Prefix LastName FirstName BirthDate(YYYYMMDD) Status");
                                for (int i = 0; i < 6; i++)
                                {
                                    str[i] = Console.ReadLine();
                                }

                                this.mLogic.New(str);
                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(2, "Üdvözlöm a felhasználókezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Felhasználók listázása", "2. Felhasználó létrehozása", "3. Felhasználó módosítása", "4. Felhasználó törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Felhasználó módosítása
                    case 23:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.mLogic.List());
                                Console.WriteLine("Enter the ID of the entry you want to change!");
                                string number = string.Empty;
                                try
                                {
                                    number = Console.ReadLine();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                WriteOut("Enter the number of field you want to change!", "1: AddressID 2: Prefix 3:FirstName 4:LastName 5:BirthDate 6:Status");
                                string field = string.Empty;
                                try
                                {
                                    field = Console.ReadLine();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                Console.WriteLine("Enter the new value of the field!");
                                this.mLogic.Change(number, field, Console.ReadLine());
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(2, "Üdvözlöm a felhasználókezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Felhasználók listázása", "2. Felhasználó létrehozása", "3. Felhasználó módosítása", "4. Felhasználó törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Felhasználó törlése
                    case 24:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.mLogic.List());
                                Console.WriteLine("Enter the ID of the entry you want to delete!");
                                int rem = 0;
                                try
                                {
                                    rem = int.Parse(Console.ReadLine());
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                try
                                {
                                    this.mLogic.Remove(rem);
                                }
                                catch (System.Data.Entity.Infrastructure.DbUpdateException)
                                {
                                    Console.WriteLine("Még van élő hivatkozás a törölni kívánt adatbázis bejegyzésre!");
                                }

                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(2, "Üdvözlöm a felhasználókezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Felhasználók listázása", "2. Felhasználó létrehozása", "3. Felhasználó módosítása", "4. Felhasználó törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Címek listázása
                    case 31:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.aLogic.List());
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(3, "Üdvözlöm a címkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Címek listázása", "2. Cím létrehozása", "3. Cím módosítása", "4. Cím törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Cím létrehozása
                    case 32:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                string[] str = new string[11];
                                WriteOut("AddressDescription City Country DoorNumber FloorNumber NumberFrom NumberTo Province Street StreetExtension ZIP");
                                for (int i = 0; i < 11; i++)
                                {
                                    str[i] = Console.ReadLine();
                                }

                                this.aLogic.New(str);
                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(3, "Üdvözlöm a címkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Címek listázása", "2. Cím létrehozása", "3. Cím módosítása", "4. Cím törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Cím módosítása
                    case 33:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.aLogic.List());
                                Console.WriteLine("Enter the ID of the entry you want to change!");
                                string number = string.Empty;
                                try
                                {
                                    number = Console.ReadLine();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                WriteOut("Enter the number of field you want to change!", "1:AddressDescription 2:City 3:Country 4:DoorNumber 5:FloorNumber 6:NumberFrom 7:NumberTo 8: Province 9:Street 10:StreetExtension 11: ZIP");
                                string field = string.Empty;
                                try
                                {
                                    field = Console.ReadLine();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                Console.WriteLine("Enter the new value of the field!");
                                this.aLogic.Change(number, field, Console.ReadLine());
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(3, "Üdvözlöm a címkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Címek listázása", "2. Cím létrehozása", "3. Cím módosítása", "4. Cím törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Cím törlése
                    case 34:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.aLogic.List());
                                Console.WriteLine("Enter the ID of the entry you want to delete!");
                                int rem = 0;
                                try
                                {
                                    rem = int.Parse(Console.ReadLine());
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                try
                                {
                                    this.aLogic.Remove(rem);
                                }
                                catch (System.Data.Entity.Infrastructure.DbUpdateException)
                                {
                                    Console.WriteLine("Még van élő hivatkozás a törölni kívánt adatbázis bejegyzésre!");
                                }

                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(3, "Üdvözlöm a címkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Címek listázása", "2. Cím létrehozása", "3. Cím módosítása", "4. Cím törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Készletek listázása
                    case 41:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.sLogic.List());
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(4, "Üdvözlöm a készletkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Készletek listázása", "2. Készlet létrehozása", "3. Készlet módosítása", "4. Készlet törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Készlet létrehozása
                    case 42:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                string[] str = new string[3];
                                WriteOut("ExpectedShipTimeInDays(int) ItemDescription(string) StockAmount(int)");
                                for (int i = 0; i < 3; i++)
                                {
                                    str[i] = Console.ReadLine();
                                }

                                this.sLogic.New(str);
                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(4, "Üdvözlöm a készletkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Készletek listázása", "2. Készlet létrehozása", "3. Készlet módosítása", "4. Készlet törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Készlet módosítása
                    case 43:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.sLogic.List());
                                Console.WriteLine("Enter the ID of the entry you want to change!", "1:ExpectedShipTimeInDays 2:ItemDescription 3:StockAmount");
                                string number = string.Empty;
                                try
                                {
                                    number = Console.ReadLine();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                WriteOut("Enter the number of field you want to change!", "1:ExpectedShipTimeInDays(int) 2:ItemDescription(string) 3:StockAmount(int)");
                                string field = string.Empty;
                                try
                                {
                                    field = Console.ReadLine();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                Console.WriteLine("Enter the new value of the field!");
                                this.sLogic.Change(number, field, Console.ReadLine());
                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(4, "Üdvözlöm a készletkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Készletek listázása", "2. Készlet létrehozása", "3. Készlet módosítása", "4. Készlet törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;

                    // Készlet törlése
                    case 44:
                        key = Console.ReadKey(true);
                        switch (key.Key)
                        {
                            case ConsoleKey.D1:
                                WriteOut(this.sLogic.List());
                                Console.WriteLine("Enter the ID of the entry you want to delete!");
                                int rem = 0;
                                try
                                {
                                    rem = int.Parse(Console.ReadLine());
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }

                                try
                                {
                                    this.sLogic.Remove(rem);
                                }
                                catch (System.Data.Entity.Infrastructure.DbUpdateException)
                                {
                                    Console.WriteLine("Még van élő hivatkozás a törölni kívánt adatbázis bejegyzésre!");
                                }

                                Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
                                while (Console.ReadKey(true).Key != ConsoleKey.Escape)
                                {
                                }

                                this.Menu(0);
                                break;
                            case ConsoleKey.D5:
                                this.Menu(4, "Üdvözlöm a készletkezelő felületen!", "Kérem válasszon az alábbi menüpontok közül", "1. Készletek listázása", "2. Készlet létrehozása", "3. Készlet módosítása", "4. Készlet törlése", "5. Vissza az előző menübe", "Escape: Kilépés");
                                this.cont = false;
                                break;
                            case ConsoleKey.Escape:
                                this.Menu(-101, string.Empty, string.Empty, "\t \t Viszontlátásra!");
                                this.cont = false;
                                break;
                            default:
                                this.Menu(depth, args);
                                break;
                        }

                        break;
                }

                break;
                }
            }
        }
    }
