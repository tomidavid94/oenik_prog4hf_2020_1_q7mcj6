﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pong
{
    class PongLogic
    {

        PongModel model;
        public enum Direction { Left, Right }
        public event EventHandler RefreshScreen;
        public PongLogic(PongModel model)
        {
            this.model = model;
        }
        public void MovePad(Direction d)
        {
            if (d == Direction.Left)
            {
                model.Pad.ChangeX(-10);
            }
            else
            {
                model.Pad.ChangeX(+10);
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);  //játéktér frissítés
        }
        public void JumpPad(double x)
        {
            model.Pad.SentXY(x, model.Pad.Area.Y);
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        private bool MoveShape(MyShape shape)
        {
            bool isFaulted = false;
            shape.ChangeX(shape.Dx);
            shape.ChangeY(shape.Dy);

            if (shape.Area.Left < 0 || shape.Area.Right > Config.Width)
            {
                shape.Dx = -shape.Dx;
            }
            if (shape.Area.Top < 0 || shape.Area.IntersectsWith(model.Pad.Area) || shape.Area.IntersectsWith(model.EnemyPad1.Area) || shape.Area.IntersectsWith(model.EnemyPad2.Area) || shape.Area.IntersectsWith(model.EnemyPad3.Area))
            {
                shape.Dy = -shape.Dy;
            }
            if (shape.Area.Bottom > Config.Height)
            {
                isFaulted = true;
                shape.SentXY(shape.Area.X, Config.Height / 2);
            }
            return isFaulted;
        }
        public void MoveBall()
        {
            if (MoveShape(model.Ball))
            {
                model.Errors++;
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void AddStar()
        {
            model.Stars.Add(new Star(Config.Width / 2, Config.Height / 2, Config.BallSize / 2, 6));
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void MoveStar()
        {
            foreach (var star in model.Stars)
            {
                if (MoveShape(star))
                {
                    model.Errors++;
                }            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void MoveEnemyPad()
        {
            MoveEnemy1(model.EnemyPad1);
            MoveEnemy2(model.EnemyPad2);
            MoveEnemy3(model.EnemyPad3);
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        //public void MoveEnemy1(MyShape enemy)
        //{
        //    if (enemy.Area.IntersectsWith(model.Ball.Area))
        //    {
        //        enemy.ChangeX(+50);
        //    }
        //    if (enemy.Area.Left < 0)
        //    {
        //        EnemyMo
        //    }

        //    RefreshScreen?.Invoke(this, EventArgs.Empty);  //játéktér frissítés
        //}

        public void MoveEnemy1(MyShape enemy)
        {
            if (enemy.Area.IntersectsWith(model.Ball.Area))
            {
                enemy.ChangeX(+200);
            }
            if (enemy.Area.Left < 0)
            {
                enemy.ChangeX(+50);
            }
            if (enemy.Area.Right > Config.Width)
            {
                enemy.ChangeX(-50);
            }
            else
            {
                Random r = new Random();
                if (r.Next(1, 50) < 26)
                {
                    enemy.ChangeX(-3);
                }
                if (r.Next(1, 50) > 26)
                {
                    enemy.ChangeX(+3);
                }
            }

            RefreshScreen?.Invoke(this, EventArgs.Empty);  //játéktér frissítés
        }
        public void MoveEnemy2(MyShape enemy)
        {
            if (enemy.Area.IntersectsWith(model.Ball.Area))
            {
                enemy.ChangeX(-200);
            }
            if (enemy.Area.Left < 0)
            {
                enemy.ChangeX(+20);
            }
            if (enemy.Area.Right > Config.Width)
            {
                enemy.ChangeX(-20);
            }
            else
            {
                Random r = new Random();
                if (r.Next(1, 40) < 18)
                {
                    enemy.ChangeX(-3);
                }
                if (r.Next(1, 40) > 18)
                {
                    enemy.ChangeX(+3);
                }
            }

            RefreshScreen?.Invoke(this, EventArgs.Empty);  //játéktér frissítés
        }
        public void MoveEnemy3(MyShape enemy)
        {
            if (enemy.Area.IntersectsWith(model.Ball.Area))
            {
                enemy.ChangeX(+200);
            }
            if (enemy.Area.Left < 0)
            {
                enemy.ChangeX(+20);
            }
            if (enemy.Area.Right > Config.Width)
            {
                enemy.ChangeX(-20);
            }
            else
            {
                Random r = new Random();
                if (r.Next(1, 1000) < 500)
                {
                    enemy.ChangeX(-3);
                }
                if (r.Next(1, 1000) > 500)
                {
                    enemy.ChangeX(+3);
                }
            }

            RefreshScreen?.Invoke(this, EventArgs.Empty);  //játéktér frissítés
        }
    }

    
}
