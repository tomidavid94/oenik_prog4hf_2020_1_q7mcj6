﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace pong
{
    class PongControl : FrameworkElement
    {
        PongModel model;
        PongLogic logic;
        PongRenderer render;
        DispatcherTimer tickTimer;

        public PongControl()
        {
            Loaded += PongControl_Loaded; // += TAB ENTER
        }

        private void PongControl_Loaded(object sender, RoutedEventArgs e)
        {
            model = new PongModel();
            logic = new PongLogic(model);
            render = new PongRenderer(model);
            Window win = Window.GetWindow(this);
            if (win != null)
            {
                tickTimer = new DispatcherTimer();
                tickTimer.Interval = TimeSpan.FromMilliseconds(20);
                tickTimer.Tick += TickTimer_Tick;  //+= TAB ENTER
                tickTimer.Start();

                win.KeyDown += Win_KeyDown;  //+= TAB ENTER
                MouseLeftButtonDown += PongControl_MouseLeftButtonDown;  //+= TAB ENTER
            }
            logic.RefreshScreen += (obj, args) => InvalidateVisual();
            InvalidateVisual();
        }

        private void PongControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            logic.JumpPad(e.GetPosition(this).X);   // egér X kordinátáját átadja 
        }

        private void Win_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.Left: logic.MovePad(PongLogic.Direction.Left); break;
                case Key.Right: logic.MovePad(PongLogic.Direction.Right); break;
                case Key.Space: logic.AddStar(); break;
            }
        }

        private void TickTimer_Tick(object sender, EventArgs e)
        {
            logic.MoveBall();
            logic.MoveStar();
            logic.MoveEnemyPad();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (render != null)
            {
                render.DrawThings(drawingContext);
            }
        }
    }
}
