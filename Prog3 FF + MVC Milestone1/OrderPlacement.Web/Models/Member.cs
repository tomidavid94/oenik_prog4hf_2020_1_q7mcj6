﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OrderPlacement.Web.Models
{
    public class Member
    {
        [Display(Name ="MemberID")]
        [Required]
        public int MemberId { get; set; }
        [Display(Name ="AddressID")]
        [Required]
        public int AddressId { get; set; }
        [Display(Name ="Prefix")]
        public string Prefix { get; set; }
        [Display(Name = "FirstName")]
        public string FistName { get; set; }
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        [Display(Name = "Date")]
        public DateTime BirthDate { get; set; }
        [Display(Name = "Status")]
        public char Status { get; set; }
    }
}