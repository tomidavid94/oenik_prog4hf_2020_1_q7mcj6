﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calories.Models
{
    public class CalorieResult
    {
        public string OriginalName { get; set; }
        public int OriginalWeight { get; set; }
        public double OriginalMinutes { get; set; }
        public double BurnedCalories { get; set; }

    }
}