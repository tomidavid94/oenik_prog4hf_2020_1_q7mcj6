﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calories.Models
{
    public class CalorieInput
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public int Minutes { get; set; }
        public string Exercise { get; set; }
       
    }
    public class Exercise
    {
        public string name;
        public int calorie;
        public Exercise(string name, int calorie)
        {
            this.name = name;
            this.calorie = calorie;
        }
    }

}