﻿// <copyright file="JavaLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Logic
{
    using System;
    using System.Net;
    using System.Xml.Linq;

    /// <summary>
    /// Java endpoint access
    /// </summary>
    public class JavaLogic
    {
        private static WebClient wC = new WebClient();
        private string web = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="JavaLogic"/> class.
        /// </summary>
        public JavaLogic()
        {
        }

        /// <summary>
        /// Gets shit back
        /// </summary>
        /// <param name="itemID">id of Item</param>
        /// <param name="desc">Description of item</param>
        /// <param name="val">Expected value of item</param>
        /// <returns>Java message</returns>
        public string[] Ask(string itemID, string desc, string val)
        {
            int iD = 0, value = 0;
            try
            {
                iD = int.Parse(itemID);
                value = int.Parse(val);
            }
            catch (Exception e)
            {
                throw new Exception("Érezzük hogy ez komolytalan", e);
            }

            string[] rsp = new string[6];
            rsp[2] = "Nem megfelelő bemeneti értékek";
            if (iD > 1 && desc != null && !desc.Equals(string.Empty) && value > 0)
            {
                XDocument xDoc = XDocument.Load($"http://localhost:8080/Vizsga/Belepes?id={itemID}&desc={desc}&val={val}");

                rsp[0] = $"ID Description Values";

                rsp[1] = xDoc.Root.Element("first").Element("id").Value.ToString();
                rsp[1] += $" \"{xDoc.Root.Element("first").Element("desc").Value.ToString()}\" ";
                rsp[1] += xDoc.Root.Element("first").Element("val").Value.ToString();

                rsp[2] = xDoc.Root.Element("second").Element("id").Value.ToString();
                rsp[2] += $" \"{xDoc.Root.Element("second").Element("desc").Value.ToString()}\" ";
                rsp[2] += xDoc.Root.Element("second").Element("val").Value.ToString();

                rsp[3] = xDoc.Root.Element("third").Element("id").Value.ToString();
                rsp[3] += $" \"{xDoc.Root.Element("third").Element("desc").Value.ToString()}\" ";
                rsp[3] += xDoc.Root.Element("third").Element("val").Value.ToString();

                rsp[4] = xDoc.Root.Element("fourth").Element("id").Value.ToString();
                rsp[4] += $" \"{xDoc.Root.Element("fourth").Element("desc").Value.ToString()}\" ";
                rsp[4] += xDoc.Root.Element("fourth").Element("val").Value.ToString();

                rsp[5] = xDoc.Root.Element("fifth").Element("id").Value.ToString();
                rsp[5] += $" \"{xDoc.Root.Element("fifth").Element("desc").Value.ToString()}\" ";
                rsp[5] += xDoc.Root.Element("fifth").Element("val").Value.ToString();
            }
            else
            {
                throw new ArgumentException("Nem megfelelő bemeneti érték");
            }

            return rsp;
        }
    }
}
