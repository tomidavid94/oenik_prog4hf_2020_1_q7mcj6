﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace pong
{
    class PongRenderer
    {
        PongModel model;
        public PongRenderer(PongModel model)
        {
            this.model = model;
        }
        // 1. Builder tervezési minta
        // 2. Konstruktor hívások elkerülése
        public void DrawThings(DrawingContext ctx)
        {
            DrawingGroup dg = new DrawingGroup();
            GeometryDrawing background = new GeometryDrawing(Config.BgBrush, 
                new Pen(Config.BorderBrush,Config.BorderSize), 
                new RectangleGeometry(new Rect(0,0,Config.Width, Config.Height)));

            GeometryDrawing ball = new GeometryDrawing(Config.BallBg, 
                new Pen(Config.BallBg, 1), 
                new EllipseGeometry(model.Ball.Area));

            GeometryDrawing pad = new GeometryDrawing(Config.PadBg, 
                new Pen(Config.PadLine, 1), 
                new RectangleGeometry(model.Pad.Area));

            GeometryDrawing enemypad1 = new GeometryDrawing(Config.EnemyBg,
                new Pen(Config.PadLine, 1),
                new RectangleGeometry(model.EnemyPad1.Area));

            GeometryDrawing enemypad2 = new GeometryDrawing(Config.EnemyBg,
                new Pen(Config.PadLine, 1),
                new RectangleGeometry(model.EnemyPad2.Area));

            GeometryDrawing enemypad3 = new GeometryDrawing(Config.EnemyBg,
                new Pen(Config.PadLine, 1),
                new RectangleGeometry(model.EnemyPad3.Area));

            FormattedText formattedtext = new FormattedText(model.Errors.ToString(),System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 16, Brushes.Black);
            GeometryDrawing text = new GeometryDrawing(null, new Pen(Brushes.Red, 3), formattedtext.BuildGeometry(new Point(5,5)));

         
            dg.Children.Add(background);
            dg.Children.Add(ball);
            dg.Children.Add(pad);
            dg.Children.Add(text);
            dg.Children.Add(enemypad1);
            dg.Children.Add(enemypad2);
            dg.Children.Add(enemypad3);
            foreach (var star in model.Stars)
            {
                GeometryDrawing geostar = new GeometryDrawing(Config.BallBg, new Pen(Config.BallLine, 1), star.GetGeometry());
                dg.Children.Add(geostar);
            }
            ctx.DrawDrawing(dg);
        }
    }
}
