﻿using Calories.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Calories.Controllers
{
    public class CalorieController : Controller
    {
        // GET: /Calorie/Burn
        public ActionResult Burn()
        {
            return View("CalorieInput");
        }
        [HttpPost]
        public ActionResult Burn(CalorieInput input)
        {
            List<Exercise> exercises = new List<Exercise>();
            exercises.Add(new Exercise("Running", 1000));
            exercises.Add(new Exercise("Yoga", 400));
            exercises.Add(new Exercise("Pilates", 472));
            exercises.Add(new Exercise("Hiking", 700));
            exercises.Add(new Exercise("Swimming", 1000));
            exercises.Add(new Exercise("Bicycle", 600));
            double onehour = 60;
            double burnedcal = 0;
            foreach (var item in exercises)
            {
                if (input.Exercise == item.name)
                {
                    burnedcal = ((item.calorie / 100) * input.Weight) / ( onehour / input.Minutes);
                }
            }
            CalorieResult cr = new CalorieResult()
            {
                OriginalName = input.Name,
                OriginalWeight = input.Weight,
                OriginalMinutes = input.Minutes,
                BurnedCalories = burnedcal
            };
            return View("CalorieResult", cr);
        }
    }
}