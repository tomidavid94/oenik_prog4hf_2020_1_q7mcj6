﻿using AutoMapper;
using OrderPlacement.Data;
using OrderPlacement.Logic;
using OrderPlacement.Repository;
using OrderPlacement.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrderPlacement.Web.Controllers
{
    public class MembersApiController : ApiController
    {
        public class ApiResult  //csak egy result használata
        {
            public bool OperationResult { get; set; }
        }

        MembersLogic logic;
        IMapper mapper;

        public MembersApiController()
        {
            OrderPlacementTablesEntities dbEntity = new OrderPlacementTablesEntities();
            MembersRepository repo = new MembersRepository();
            this.logic = new MembersLogic();
            this.mapper = MapperFactory.CreateMapper();
        }

        //GET api/MembersApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Member> GetAll()
        {
            var members = logic.GetAllMembers();
            return mapper.Map<IList<OrderPlacement.Data.Members>, List<Models.Member>>(members);
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneMember(Member member)
        {
            logic.Add(member.MemberId, member.Prefix, member.FistName, member.LastName);
            return new ApiResult() { OperationResult = true };
        }

        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneMember(Member member)
        {
            bool success = logic.ChangeMember(member.MemberId, member.Prefix, member.FistName, member.LastName);
            return new ApiResult() { OperationResult = success };
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneMember(int id)
        {
            bool success = logic.Remove(id);
            return new ApiResult() { OperationResult = success };
        }
    }
}
