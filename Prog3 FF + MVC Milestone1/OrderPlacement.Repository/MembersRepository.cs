﻿// <copyright file="MembersRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OrderPlacement.Repository
{
    using System;
    using System.Linq;
    using System.Threading;
    using OrderPlacement.Data;
    using OrderPlacement.Repository.Interfaces;

    /// <summary>
    /// Repository for accessing Members
    /// </summary>
    public class MembersRepository : Repository, IMembersRepository
    {
        private OrderPlacementTablesEntities dbEntity = new OrderPlacementTablesEntities();

        /// <summary>
        /// Changes an entity in the table
        /// </summary>
        /// <param name="entity">ID of entity</param>
        /// <param name="field">Number of field</param>
        /// <param name="newfield">Value of field's new value</param>
        public void Change(Members entity, string field, string newfield)
        {
            int asd = 0;

            try
            {
                switch (field)
                {
                    case "AddressID":
                        try
                        {
                            asd = int.Parse(newfield);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            return;
                        }

                        entity.AddressID = asd;
                        break;
                    case "Prefix":
                        entity.Prefix = newfield;
                        break;
                    case "FirstName":
                        entity.FirstName = newfield;
                        break;
                    case "LastName":
                        entity.LastName = newfield;
                        break;
                    case "BirthDate":
                        DateTime date = DateTime.Today;
                        try
                        {
                            date = new DateTime(int.Parse(newfield.ToString().Substring(0, 4)), int.Parse(newfield.ToString().Substring(4, 2)), int.Parse(newfield.ToString().Substring(6, 2)));
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            break;
                        }

                        entity.Birthdate = date;
                        break;
                    case "Status":
                        entity.Status = newfield;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            this.dbEntity.SaveChanges();
        }

        /// <summary>
        /// Change Members
        /// </summary>
        /// <param name="id">id of member</param>
        /// <param name="prefix">prefix of member</param>
        /// <param name="firstName">firstname of member</param>
        /// <param name="lastName">lastname of member</param>
        /// <returns>true or false</returns>
        public bool ChangeMember(int id, string prefix, string firstName, string lastName)
        {
            Members entity = this.GetOne(id);
            if (entity == null)
            {
                return false;
            }
            else
            {
                entity.MemberID = id;
                entity.Prefix = prefix;
                entity.FirstName = firstName;
                entity.LastName = lastName;
                this.dbEntity.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// Returns all Members as queryable
        /// </summary>
        /// <returns>All members</returns>
        public IQueryable<Members> GetAll()
        {
            return this.dbEntity.Set<Members>();
        }

        /// <summary>
        /// Inserts a new Member into the table
        /// </summary>
        /// <param name="entity">Inserted entity</param>
        public void Insert(Members entity)
        {
            this.dbEntity.Members.Add(entity);
            this.dbEntity.SaveChanges();
            //try
            //{
            //    this.dbEntity.SaveChanges();
            //}

            //catch (Exception e)

            //{
            //    Console.WriteLine(e.Message);
            //    if (e.InnerException != null)
            //    {
            //        if (e.InnerException.InnerException != null)
            //        {
            //            Console.WriteLine(e.InnerException.InnerException.Message);
            //            Console.WriteLine("Nyomja meg az Escape gombot a főmenübe való visszalépéshez!");
            //            while (Console.ReadKey(true).Key != ConsoleKey.Escape)
            //            {
            //            }
            //        }
            //    }
            //}

        }

        void IRepository<Members>.Remove(Members entity)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get One Member
        /// </summary>
        /// <param name="id">Get one Membeer</param>
        /// <returns>Selected member</returns>
        public Members GetOne(int id)
        {
            foreach (var item in this.dbEntity.Members)
            {
                if (item.MemberID == id)
                {
                    return item;
                }
            }

            return null;
        }

        /// <summary>
        /// Remove a Member from the table
        /// </summary>
        /// <param name="entity">Removed Members</param>
        /// <returns>result of the remove action</returns>
        public bool Remove(Members entity)
        {
            if (entity == null)
            {
                return false;
            }
            else
            {
                this.dbEntity.Members.Remove(entity);
                this.dbEntity.SaveChanges();
                return true;
            }
        }


    }
}
