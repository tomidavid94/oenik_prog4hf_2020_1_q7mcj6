﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OrderPlacement.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:50321/api/MembersApi/";
        HttpClient client = new HttpClient();

        void SendMassege(bool success)
        {
            string msg = success ? "Operation completed" : "Operation failed";
            Messenger.Default.Send(msg, "MemberResult");
        }

        public List<MemberVM> ApiGetMembers()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<MemberVM>>(json);
            return list;
        }
        public void ApiDelMember(MemberVM member)
        {
            bool success = false;
            if (member != null)
            {
                string json = client.GetStringAsync(url + "del/" + member.MemberId).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMassege(success);
        }

        bool ApiEditMember(MemberVM member, bool isEditing)
        {
            if (member == null)
            {
                return false;
            }
            string myurl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(MemberVM.MemberId), member.MemberId.ToString());
            }
            postData.Add(nameof(MemberVM.Prefix), member.Prefix);
            postData.Add(nameof(MemberVM.Fistname), member.Fistname);
            postData.Add(nameof(MemberVM.Lastname), member.Lastname);

            string json = client.PostAsync(myurl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }


        public void EditMember(MemberVM member, Func<MemberVM, bool> editor)
        {
            MemberVM clone = new MemberVM();
            if (member != null)
            {
                clone.CopyFrom(member);
            }
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (member != null)
                {
                    success = ApiEditMember(clone, true);
                }
                else
                {
                    success = ApiEditMember(clone, false);
                }
            }
            SendMassege(success == true);
        }
    }
}
