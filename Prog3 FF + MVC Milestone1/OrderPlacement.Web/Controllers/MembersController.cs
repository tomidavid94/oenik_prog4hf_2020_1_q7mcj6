﻿using AutoMapper;
using OrderPlacement.Logic;
using OrderPlacement.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrderPlacement.Data;
using System.Web.Mvc;
using OrderPlacement.Repository;

namespace OrderPlacement.Web.Controllers
{
    public class MembersController : Controller
    {
        MembersLogic logic;
        IMapper mapper;
        MembersViewModel vm;

        public MembersController()
        {
            OrderPlacementTablesEntities dbEntity = new OrderPlacementTablesEntities();
            MembersRepository repo = new MembersRepository();
            this.logic = new MembersLogic();
            this.mapper = MapperFactory.CreateMapper();
            vm = new MembersViewModel();
            vm.EditedMember = new Member();
            var members = logic.GetAllMembers();
            vm.ListofMembers = mapper.Map<IList<OrderPlacement.Data.Members>, List<Models.Member>>(members);
        }

        private Member GetMemberModel(int id)
        {
            Members oneCar = logic.GetOne(id);
            return mapper.Map<OrderPlacement.Data.Members, Models.Member>(oneCar);
        }

        // GET: Members
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("MemberIndex", vm);
        }

        // GET: Members/Details/5
        public ActionResult Details(int id)
        {
            return View("MemberDetails", GetMemberModel(id));
        }

        // GET: Members/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedMember = GetMemberModel(id);
            return View("MemberIndex", vm);
        }

        // POST: Members/Edit/5
        [HttpPost]
        public ActionResult Edit(Member member, string editAction)
        {
            if (ModelState.IsValid && member != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.Add(member.MemberId, member.Prefix, member.FistName, member.LastName);
                }
                else
                {
                    bool success = logic.ChangeMember(member.MemberId, member.Prefix, member.FistName, member.LastName);
                    if (!success)
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedMember = member;
                return View("MemberIndex", vm);
            }
        }

        // GET: Members/Delete/5
        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete Fail";
            if (logic.Remove(id))
            {
                TempData["editResult"] = "Delete OK";
            }
            return RedirectToAction("Index");
        }
        
    }
}
