﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderPlacement.Web.Models
{
    public class MembersViewModel
    {
        public Member EditedMember { get; set; }
        public List<Member> ListofMembers { get; set; }
    }
}